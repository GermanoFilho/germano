package br.com.germano.desafioandroid.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import br.com.germano.desafioandroid.R;
import br.com.germano.desafioandroid.entities.Pull;
import br.com.germano.desafioandroid.services.PullService;
import br.com.germano.desafioandroid.ui.adapters.PullAdapter;
import br.com.germano.desafioandroid.utils.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullActivity extends BaseActivity {
    ImageView btnBack;
    Toolbar toolbar;
    private List<Pull> pullRequests;
    LinearLayoutManager mLayoutManager;
    private String repo, owner;
    View contentView;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.activity_pull, null, false);
        drawer.addView(contentView, 0);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.title_activity_pull);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //getting info from previous activity
        Intent intent = getIntent();
        owner = intent.getExtras().getString("owner");
        repo = intent.getExtras().getString("repo");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        callPullService();
    }

    public void callPullService(){
        showDialog();
        PullService service = RetrofitUtil.getInstance().apiBuild().create(PullService.class);
        Call<List<Pull>> call = service.pullRequests(owner, repo);

        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if(response.isSuccessful()){
                    pullRequests = response.body();
                    recyclerView.setAdapter(new PullAdapter(getApplicationContext(), pullRequests));
                    hideDialog();
                } else {
                    hideDialog();
                    errorHandle(contentView);
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                hideDialog();
                errorHandle(contentView);
            }
        });
    }
}
