package br.com.germano.desafioandroid.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Inception on 20/01/2017.
 */

public class Repository {

    @SerializedName("total_count")
    private Long totalCount;

    @SerializedName("incomplete_result")
    private Boolean isIncompleteResult;

    @SerializedName("items")
    private List<Item> items;

    public Long getTotalCount() {
        return totalCount;
    }

    public Boolean getIncompleteResult() {
        return isIncompleteResult;
    }

    public List<Item> getItems() {
        return items;
    }
}
