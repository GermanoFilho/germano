package br.com.germano.desafioandroid.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.germano.desafioandroid.R;
import br.com.germano.desafioandroid.entities.Item;
import br.com.germano.desafioandroid.ui.activities.PullActivity;
import br.com.germano.desafioandroid.utils.CircleTransform;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Inception on 20/01/2017.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder>{
    private List<Item> items;
    private Context context;

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_repository) TextView title;
        @BindView(R.id.description_repository) TextView description;
        @BindView(R.id.text_fork) TextView fork;
        @BindView(R.id.text_star) TextView star;
        @BindView(R.id.avatar) ImageView avatar;
        @BindView(R.id.username) TextView userName;
        @BindView(R.id.linear_repository) LinearLayout linearRepository;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public RepositoryAdapter(Context context, List<Item> repositoryItems) {
        this.items = repositoryItems;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_repository, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Item item = items.get(position);
        holder.title.setText(item.getName());
        holder.description.setText(item.getDescription());
        holder.fork.setText(item.getForksCount().toString());
        holder.star.setText(item.getStargazersCount().toString());
        holder.userName.setText(item.getFullName());

        Picasso.with(context)
                .load(item.getOwner().getAvatarUrl())
                .placeholder(R.drawable.placeholder)
                .transform(new CircleTransform())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(context)
                                .load(item.getOwner().getAvatarUrl())
                                .placeholder(R.drawable.placeholder)
                                .transform(new CircleTransform())
                                .into(holder.avatar);
                    }
                });


        holder.linearRepository.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PullActivity.class);
                intent.putExtra("owner",item.getName());
                intent.putExtra("repo", item.getOwner().getLogin());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



}
