package br.com.germano.desafioandroid.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Inception on 20/01/2017.
 */

public class Item {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("owner")
    private Owner owner;

    @SerializedName("private")
    private Boolean isPrivate;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("description")
    private String description;

    @SerializedName("stargazers_count")
    private Integer stargazersCount;

    @SerializedName("forks_count")
    private Integer forksCount;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public Owner getOwner() {
        return owner;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStargazersCount() {
        return stargazersCount;
    }

    public Integer getForksCount() {
        return forksCount;
    }
}
