package br.com.germano.desafioandroid.utils;
import br.com.germano.desafioandroid.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Inception on 20/01/2017.
 */

public final class RetrofitUtil {

    public static RetrofitUtil instance;

    private RetrofitUtil() {
    }

    public static RetrofitUtil getInstance() {
        if (instance == null) {
            instance = new RetrofitUtil();
        }
        return instance;
    }

    public Retrofit apiBuild() {
        return new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
