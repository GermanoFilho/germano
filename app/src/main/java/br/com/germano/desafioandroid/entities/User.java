package br.com.germano.desafioandroid.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Inception on 20/01/2017.
 */

public class User {

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
