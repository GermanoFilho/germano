package br.com.germano.desafioandroid.services;

import br.com.germano.desafioandroid.entities.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Inception on 20/01/2017.
 */

public interface RepositoryService {

    @GET("search/repositories")
    Call<Repository> getRepositories(@Query("q") String language, @Query("sort") String sort, @Query("page") int page);

}
