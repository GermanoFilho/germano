package br.com.germano.desafioandroid.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Inception on 20/01/2017.
 */

public class Pull {
    @SerializedName("title")
    private String title;

    @SerializedName("user")
    private User user;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String created;

    public String getTitle() {
        return title;
    }

    public User getUser() {
        return user;
    }

    public String getBody() {
        return body;
    }

    public String getCreated() {
        return created;
    }
}
