package br.com.germano.desafioandroid.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.germano.desafioandroid.R;
import br.com.germano.desafioandroid.entities.Pull;
import br.com.germano.desafioandroid.utils.CircleTransform;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Inception on 20/01/2017.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolder>{
    private List<Pull> pullRequests;
    private Context context;

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title_pull) TextView title;
        @BindView(R.id.description_pull) TextView description;
        @BindView(R.id.avatar) ImageView avatar;
        @BindView(R.id.username) TextView userName;
        @BindView(R.id.date) TextView date;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public PullAdapter(Context context, List<Pull> pullRequests) {
        this.pullRequests = pullRequests;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pull, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Pull pull= pullRequests.get(position);
        holder.title.setText(pull.getTitle());
        holder.description.setText(pull.getBody());
        holder.userName.setText(pull.getUser().getLogin());
        holder.date.setText(pull.getCreated().substring(0, 10).replace("-", "/"));

        Picasso.with(context)
                .load(pull.getUser().getAvatarUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.placeholder)
                .transform(new CircleTransform())
                .into(holder.avatar, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        Picasso.with(context)
                                .load(pull.getUser().getAvatarUrl())
                                .transform(new CircleTransform())
                                .placeholder(R.drawable.placeholder)
                                .into(holder.avatar);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }
}
