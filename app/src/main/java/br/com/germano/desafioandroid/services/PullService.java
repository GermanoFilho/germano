package br.com.germano.desafioandroid.services;

import java.util.List;

import br.com.germano.desafioandroid.entities.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Inception on 20/01/2017.
 */

public interface PullService {

    @GET("repos/{repo}/{owner}/pulls")
    Call<List<Pull>> pullRequests(@Path("owner") String owner, @Path("repo") String repo);
}
