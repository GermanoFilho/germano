package br.com.germano.desafioandroid.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.List;

import br.com.germano.desafioandroid.R;
import br.com.germano.desafioandroid.entities.Item;
import br.com.germano.desafioandroid.entities.Repository;
import br.com.germano.desafioandroid.services.RepositoryService;
import br.com.germano.desafioandroid.ui.adapters.RepositoryAdapter;
import br.com.germano.desafioandroid.utils.RetrofitUtil;
import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    private List<Item> items = new ArrayList<>();
    LinearLayoutManager mLayoutManager;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    int maxItemsPerRequest = 30;
    int position = 0;
    View contentView;
    int page = 1;
    boolean lastPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(R.layout.activity_main, null, false);
        drawer.addView(contentView, 0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toggle(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(createInfiniteScrollListener());
        callRepositoryService(page);
    }

    public void callRepositoryService(final int page){
        showDialogBar(page);
        RepositoryService service = RetrofitUtil.getInstance().apiBuild().create(RepositoryService.class);
        Call<Repository> call = service.getRepositories("java", "stars", page);
        call.enqueue(new Callback<Repository>() {
            @Override
            public void onResponse(Call<Repository> call, Response<Repository> response) {
                if(response.isSuccessful()){
                    if(page > 1){
                        items.addAll(response.body().getItems());
                        recyclerView.setAdapter(new RepositoryAdapter(MainActivity.this, items));
                        recyclerView.invalidate();
                        recyclerView.scrollToPosition(position);
                        hideDialogBar(page);
                    } else {
                        items =  response.body().getItems();
                        recyclerView.setAdapter(new RepositoryAdapter(getApplicationContext(), items));
                        hideDialogBar(page);
                    }

                } else {
                    if(response.code() == 422){
                        hideDialogBar(page);
                        lastPage = true;
                    } else {
                        hideDialogBar(page);
                        errorHandle(contentView);
                    }

                }
            }

            @Override
            public void onFailure(Call<Repository> call, Throwable t) {
                hideDialog();
                errorHandle(contentView);
            }
        });
    }

    private InfiniteScrollListener createInfiniteScrollListener() {
        return new InfiniteScrollListener(maxItemsPerRequest, mLayoutManager) {
            @Override
            public void onScrolledToEnd(final int firstVisibleItemPosition) {
                position = firstVisibleItemPosition;
                if(progressBar.getVisibility() == View.GONE){
                    if(!lastPage){
                        page++;
                        callRepositoryService(page);
                    }
                }
            }
        };
    }

    //verify if need show progressDialog or progressBar to infinite scroll
    private void showDialogBar(int page){
        if(page == 1){
            showDialog();
        } else {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    //verify if need hide progressDialog or progressBar to infinite scroll
    private void hideDialogBar(int page){
        if(page == 1){
            hideDialog();
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
