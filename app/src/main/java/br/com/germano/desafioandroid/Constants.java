package br.com.germano.desafioandroid;

/**
 * Created by Inception on 20/01/2017.
 */

public interface Constants {

    String API_URL = "https://api.github.com/";
    String MY_PHOTO_URL = "https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAALSAAAAJGVhMDViZjBiLWNkMmQtNDdlNS1iNTUzLWY0YjBiYTkxMDYzYg.jpg";
    String WEBSITE = "http://germanofilho.com/";
    String GOOGLE_PLAY ="https://play.google.com/store/apps/developer?id=GF+Mobile";
}
